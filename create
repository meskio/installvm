#!/bin/bash

ROOT_PATH=.
DOMAIN=sindominio.net
VARIANT=debian10
ISOPATH=${ROOT_PATH}/debian-10.6.0-amd64-netinst.iso
RAM=1024  # in MB
DISK=32   # in GB
INSTALLPKGS="openssh-server vim tmux unattended-upgrades"
TMP_PATH=$(mktemp -d /tmp/vm-create.XXXXXX)

show_help() {
cat << EOF
Usage: $0 [OPTION] <name_vm> <num_vm>
Options:
  -h,--help			display this help message
  -r,--ram	<number>	the memory size asigned to the VM in MB
  -d,--disk 	<number>	the disk size for the VM in GB
EOF
}

while [[ $1 == -* ]]
do
	case "$1" in
		-h|--help|-\?)
			show_help
			exit 0
			;;
		-r|--ram)
			RAM=$2
			shift
			shift
			;;
		-d|--disk)
			DISK=$2
			shift
			shift
			;;
		-*)
			echo "invalid option: $1" 1>&2
			show_help
			exit 1
			;;
	esac
done

if [ $# -lt 2 ]
then

	echo "you need to provide a name and a number for the VM" 1>&2
	show_help
	exit 1
fi

HOSTNAME=$1
VMNUM=$2

sed "s/@@VMNUM@@/${VMNUM}/g; \
     s/@@HOSTNAME@@/${HOSTNAME}/g; \
     s/@@DOMAIN@@/${DOMAIN}/g; \
     s/@@INSTALLPKGS@@/${INSTALLPKGS}/g;" \
    ${ROOT_PATH}/preseed.tmpl > ${TMP_PATH}/preseed.cfg

tar cvf ${TMP_PATH}/postinst.tar -C ${ROOT_PATH} postinst

virt-install --debug \
--connect=qemu:///system \
--name=${HOSTNAME} \
--ram=${RAM} \
--vcpus=1 \
--disk size=${DISK},path=/srv/vm/kvm/disks/${HOSTNAME}.qcow2,format=qcow2,bus=virtio,cache=none \
--initrd-inject=${TMP_PATH}/preseed.cfg \
--initrd-inject=${TMP_PATH}/postinst.tar \
--initrd-inject=${ROOT_PATH}/postinst.sh \
--location ${ISOPATH} \
--os-type linux \
--os-variant ${VARIANT} \
--virt-type=kvm \
--controller usb,model=none \
--graphics none \
--noautoconsole \
--network bridge=br1,model=virtio \
--extra-args="auto=true hostname="${HOSTNAME}" domain="${DOMAIN}" console=tty0 console=ttyS0,115200n8 serial"

echo "192.168.23.${VMNUM}	${HOSTNAME}.sindominio.net	${HOSTNAME}" >> /etc/dnsmasq.hosts
systemctl reload dnsmasq
rm -rf ${TMP_PATH}

echo ""
echo "Connect to see the installation:"
echo "  # virsh console ${HOSTNAME}"
