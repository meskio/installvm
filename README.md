Setup VMs automatically.

Usage
-----

Get the debian iso to install, and ajust if needed the `ISOPATH` in `create`. Or put all the assets in a path and configure the `ROOT_PATH` variable.

Copy the ssh public key that you will use in the postinst folder:
```
$ cp ~/.ssh/id_rsa.pub postinst/authorized_keys
```

Install the VM providing the hostname and the VM number (the last block of the IP):
```
$ ./create maquineta 42
```

Profit!!! Everything should work automatically now. You can watch the installer running by connecting to the console:
```
$ virsh console maquineta
```

Params
------

The memory can be set with `-r` (in MB) and the disk space with `-d` (in GB).

Credits
-------

This repo has a lot of copypasta from:
https://github.com/pin/debian-vm-install
